//
//  CustomButton.h
//  事件响应机制
//
//  Created by 伍银荣 on 2020/7/6.
//  Copyright © 2020 com.example. All rights reserved.
//

#import <UIKit/UIKit.h>

NS_ASSUME_NONNULL_BEGIN

@interface CustomButton : UIButton

@end

NS_ASSUME_NONNULL_END
