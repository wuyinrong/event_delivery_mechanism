//
//  ViewController.m
//  事件响应机制
//
//  Created by 伍银荣 on 2020/7/6.
//  Copyright © 2020 com.example. All rights reserved.
//

#import "ViewController.h"
#import "CustomButton.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    CustomButton *customButton = [[CustomButton alloc]initWithFrame:CGRectMake(100, 100, 120, 120)];
    customButton.backgroundColor = [UIColor redColor];
    [customButton addTarget:self action:@selector(doAction) forControlEvents:UIControlEventTouchUpInside];
    [self.view addSubview:customButton];
    
    
    
}

- (void)doAction {
    NSLog(@"----clicked-----");
}


@end
