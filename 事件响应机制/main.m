//
//  main.m
//  事件响应机制
//
//  Created by 伍银荣 on 2020/7/6.
//  Copyright © 2020 com.example. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
